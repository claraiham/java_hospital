import java.util.ArrayList;

public class Hospital {
    public static void main(String[] args) {

        ArrayList<Medication> medicationListVaricelle = new ArrayList<>(); // tableau de médicaments pour une maladie

        Medication aciclovir = new Medication("aciclovir", "250mg");// création du traitement

        medicationListVaricelle.add(aciclovir); // ajout du traitement au tableau medicaments varicelle

        ArrayList<Illness> illnessListToto = new ArrayList<>(); // tableau de maladies de toto

        Illness varicelle = new Illness("varicelle", medicationListVaricelle); // création d'une maladie
        
        illnessListToto.add(varicelle); // ajout de la maladie au tableau de maladies de toto

        Patient toto = new Patient("Toto", 56, "1 65768978970767", "id1", illnessListToto); // création d'un patient

        System.out.println(toto.getInfo());// les infos de toto

        Doctor chuckNorris = new Doctor("Chuck Norris", 70, "1 67890987654", "id56", "Vétérinaire"); 

        System.out.println(chuckNorris.getRole());
        System.out.println(chuckNorris.careForPatient(toto));

        Nurse Giselle = new Nurse("Giselle", 60, "2 34567898765", "id98765");

        Patient baby = new Patient("Siegfried", 0, "1 876574535678", "id8972");

        System.out.println(Giselle.getRole());
        System.out.println(Giselle.careForPatient(baby));
    }
}
