public class Doctor extends MedicalStaff{

    private String specialty;

    public Doctor(String nameValue, int ageValue, String socialSecurityNumberValue, String employeeId, String specialty) {
        super(nameValue, ageValue, socialSecurityNumberValue, employeeId);
        this.specialty = specialty;
    }

    @Override
    public String getRole(){
        return "Role de "+ getName() +": Docteur";
    }

    @Override
    public String careForPatient(Patient patient){
        return this.specialty + " " + getName() + " cares for "+ patient.getName();
    }
}
