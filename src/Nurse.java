public class Nurse extends MedicalStaff{

    public Nurse(String nameValue, int ageValue, String socialSecurityNumberValue, String employeeId) {
        super(nameValue, ageValue, socialSecurityNumberValue, employeeId);
        //TODO Auto-generated constructor stub
    }

    @Override
    public String careForPatient(Patient patient) {
        return getName() + " cares for "+ patient.getName();
    }

    @Override
    public String getRole() {
        return "Role de "+ getName() +": Nurse";
    }
    
}
