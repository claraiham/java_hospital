public abstract class Person {
    private String name;
    private int age;
    private String socialSecurityNumber;

    public Person(String nameValue, int ageValue, String socialSecurityNumberValue){
        this.name = nameValue;
        this.age = ageValue;
        this.socialSecurityNumber = socialSecurityNumberValue;
    }

    public String getName(){
        return this.name;
    }

    public int getAge(){
        return this.age;
    }

    public String getSocialSecurity(){
        return this.socialSecurityNumber;
    }
}
