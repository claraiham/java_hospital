public abstract class MedicalStaff extends Person implements Care {
    private String employeeId;

    public MedicalStaff(String nameValue, int ageValue, String socialSecurityNumberValue, String employeeId) {
        super(nameValue, ageValue, socialSecurityNumberValue);
        this.employeeId = employeeId;
    }

    public abstract String getRole();
}
