import java.util.ArrayList;

public class Illness {
    private String name;
    private ArrayList<Medication> medicationList = new ArrayList<Medication>();

    public Illness(String nameValue){
        this.name = nameValue;
    }
    // surcharge pour + de possibilités
    public Illness(String nameValue,ArrayList<Medication> medicationList){
        this.name = nameValue;
        this.medicationList = medicationList;
    }

    public void addMedication(Medication medication){
        medicationList.add(medication);
    }

    public String getMedicationList(){
        String listOfMedicament = "";
        for (int i = 0; i < medicationList.size(); i++) {
            listOfMedicament += medicationList.get(i).getMedicationInfos() + " ";
        }
        return listOfMedicament;
    }

    public String getIllnessInfo(){
        return "Maladie: "+ this.name + ", "+ "médicaments: " + getMedicationList();
    }
}
