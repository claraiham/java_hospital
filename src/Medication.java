public class Medication {
    private String name;
    private String dosage;

    public Medication( String nameMedication, String dosage){
        this.name = nameMedication;
        this.dosage = dosage;
    }

    public String getMedicationInfos(){
        return this.name + ", " + "dosage: "+ this.dosage;
    }
}
