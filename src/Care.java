public interface Care {
    public String careForPatient(Patient patient);
    default public String recordPatientVisit(String notes){
        return notes;
    };
}
