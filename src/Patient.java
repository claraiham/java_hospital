import java.util.ArrayList;

public class Patient extends Person {
    private String patientId;
    private ArrayList<Illness> illnessList = new ArrayList<Illness>();// liste vide

    public Patient(String nameValue, int ageValue, String socialSecurityNumberValue, String patientId, ArrayList<Illness> illnessList) {
        super(nameValue, ageValue, socialSecurityNumberValue);
        this.patientId = patientId;
        this.illnessList = illnessList;
    }

    public Patient(String nameValue, int ageValue, String socialSecurityNumberValue, String patientId) {
        super(nameValue, ageValue, socialSecurityNumberValue);
        this.patientId = patientId;
    }

    public void addIllness(Illness illness){
        illnessList.add(illness);
    }

    public String getIllnessList(){
        String listOfIllness = "";
        for (int i = 0; i < illnessList.size(); i++) {
            listOfIllness += illnessList.get(i).getIllnessInfo() + " ";
        }
        return listOfIllness;
    }

    public String getInfo(){
        return "patient n°"+ patientId + "\n" + getName() + "\n" + getAge()+ "\n" + getSocialSecurity() + "\n" + getIllnessList();
    }
}
